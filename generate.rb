#Brandan Corthell
#bcorthell@cityofgreenriver.org

#Read hosts and ip addresses into Array from two column space delineated File

hosts = []
File.open('book1.csv') do |f|
  f.each_line do |line|
    hosts << line.split(",")
  end
end

hosts.each do |nodename, address, objecttype|
    target = open("#{nodename}.conf","w")
    objecttype = objecttype.chomp
    target.write("object Host \"#{nodename}\" {")
    target.write("\n")
    target.write('   check_command = "hostalive"')
    target.write("\n")
    target.write("   address = \"#{address}\" ")
    target.write("\n")
    target.write("   vars.objecttype = \"#{objecttype}\" ")
    target.write("\n")
    target.write("}")
end
